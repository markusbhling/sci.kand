import { KandPage } from './app.po';

describe('kand App', function() {
  let page: KandPage;

  beforeEach(() => {
    page = new KandPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
