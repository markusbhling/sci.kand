import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from "./user-routing.module";
import {LoginComponent} from "./login/login.component";
import {UserService} from "./user.service";
import {FormsModule} from "@angular/forms";
import {AuthGuard} from "./auth-guard.service";

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: [
    UserService,
    AuthGuard
  ]
})
export class UserModule { }
