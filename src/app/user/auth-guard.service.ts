import {Injectable}     from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot}    from '@angular/router';
import {UserService} from "./user.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router) {
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let redirectUrl: string = state.url;

    return this.checkLogin(redirectUrl);
  }

  checkLogin(redirectUrl: string): boolean {
    if (this.userService.isAuthenticated) {
      return true;
    }
    // Navigate to the login page with extras
    this.router.navigate(['/login'], {queryParams: {redirectUrl}});
    return false;
  }
}
