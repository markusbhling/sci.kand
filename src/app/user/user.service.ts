import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {User} from "./user.model";
import {Router} from "@angular/router";

import 'rxjs/Rx';


@Injectable()
export class UserService {

  public user : User;
  public isAuthenticated : boolean = false;

  private loginUrl = "/api/login";

  constructor(private http : Http, private router : Router) {}

  authenticate(username: string, password : string) : Observable<User> {
    let data = new FormData();
    data.append('username', username);
    data.append('password', password);
    return this
      .http
      .post(this.loginUrl, data)
      .map((res) => {
        let responseData = res.json();
        if(!responseData.user) { throw responseData.message }
        return new User(res.json().username);
      });
  }

  logout() : void {
    this.user = null;
    this.isAuthenticated = false;
    this.router.navigate(['/']);
  }
  private extractData(res: Response) {
    let body = res.json();
    return body || [];
  }
}
