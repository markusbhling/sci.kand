import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {debug} from "util";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  private username : string;
  private password : string;

  private error : string;

  constructor(private userService : UserService, private router : Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  submit() : void {
    this.error = null;
    this.userService.authenticate(this.username, this.password)
      .subscribe(
        (user) => {
          this.userService.isAuthenticated = true;
          this.activatedRoute
            .queryParams
            .subscribe(
              (params : any) => {
                this.userService.isAuthenticated = true;
                this.router.navigate([params.redirectUrl || '/']);
              }
            );

        },
        (error) => {
          this.error = error;
          this.userService.isAuthenticated = false;
        }
      );
  }

}
