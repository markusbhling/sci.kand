import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Building} from "./building/building.model";


@Injectable()
export class BuildingsService {

  private buildingsUrl = "/api/buildings";

  constructor(private http : Http) {}

  getBuildings() : Observable<Building[]> {
    return this.http.get(this.buildingsUrl)
      .map(this.extractData);
  }

  getBuilding(id : number) : Observable<Building> {
    return this.http.get(this.buildingsUrl + '/' + id)
      .map(this.extractData);
  }

  saveBuilding(building : Building) : Observable<Building> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(building);
    return this.http
      .put(this.buildingsUrl + '/' + building.building_id, body, options)
      .map((res: Response) => res.json());
  }

  createBuilding(building : Building) : Observable<Building> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(building);
    return this.http
      .post(this.buildingsUrl, body, options)
      .map((res: Response) => res.json());
  }


  private extractData(res: Response) {
    let body = res.json();
    return body || [];
  }
}
