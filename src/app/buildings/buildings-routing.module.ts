import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import {BuildingsComponent} from "./buildings.component";
import {AuthGuard} from "../user/auth-guard.service";
import {BuildingComponent} from "./building/building.component";
import {BuildingNewComponent} from "./building/building-new.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'buildings/new',
        component: BuildingNewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'buildings/:id',
        component: BuildingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'buildings',
        component: BuildingsComponent,
        canActivate: [AuthGuard]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class BuildingsRoutingModule {}
