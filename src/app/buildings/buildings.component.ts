import {Component, OnInit} from '@angular/core';
import {BuildingsService} from "./buildings.service";
import {Building} from "./building/building.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.sass']
})
export class BuildingsComponent implements OnInit {

  errorMessage: string;
  buildings: Building[];

  constructor(private buildingsService: BuildingsService, private router : Router) {
  }

  ngOnInit() {
    this.getBuildings();
  }

  getBuildings() {
    this.buildingsService.getBuildings()
      .subscribe(
        buildings => this.buildings = buildings,
        error => this.errorMessage = <any>error);
  }

  selectBuilding(building : Building) {
    this.router.navigate(['/buildings', building.building_id]);
  }

  addBuilding() {
    this.router.navigate(['/buildings/new']);
  }

}
