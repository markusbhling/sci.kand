import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BuildingsRoutingModule} from "./buildings-routing.module";
import {BuildingsComponent} from "./buildings.component";
import {HttpModule} from "@angular/http";
import {BuildingsService} from "./buildings.service";
import {BuildingComponent} from "./building/building.component";
import {AreasModule} from "../areas/areas.module";
import {BuildingEditComponent} from "./building/building-edit.component";
import {FormsModule} from "@angular/forms";
import {BuildingNewComponent} from "./building/building-new.component";

@NgModule({
  imports: [
    CommonModule,
    BuildingsRoutingModule,
    HttpModule,
    AreasModule,
    FormsModule
  ],
  declarations: [
    BuildingsComponent,
    BuildingComponent,
    BuildingEditComponent,
    BuildingNewComponent
  ],
  providers: [
    BuildingsService
  ]
})
export class BuildingsModule { }
