import { Injectable }             from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {Building} from "./building.model";
import {BuildingsService} from "../buildings.service";
import {Observable} from "rxjs";
@Injectable()
export class BuildingResolve implements Resolve<Building> {
  constructor(private buildingsService: BuildingsService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<Building> | boolean {
    let id = +route.params['id'];
    return this.buildingsService.getBuilding(id);
  }
}
