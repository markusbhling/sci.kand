import {Component, OnInit} from '@angular/core';
import {BuildingsService} from "../buildings.service";
import {Building} from "./building.model";
import {ActivatedRoute, Params} from "@angular/router";
import {Area} from "../../areas/area/area.model";
import {AreasService} from "../../areas/areas.service";

@Component({
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.sass']
})
export class BuildingComponent implements OnInit {

  building: Building;
  areas: Area[];
  errorMessage : string;
  editing : boolean = false;

  constructor(
    private buildingsService: BuildingsService,
    private areasService : AreasService,
    private activatedRoute : ActivatedRoute) {
  }

  ngOnInit() {
    this.areas = [];
    this.activatedRoute
      .params
      .forEach(
        (params : Params) => this.getBuilding(+params['id'])
      );

  }

  getBuilding(id : number) {
    this.buildingsService.getBuilding(id)
      .subscribe(
        building => {
          this.building = building;
          this.areasService
            .getAreaByParentId(building.area_id.toString())
            .subscribe(
              areas => this.areas = areas,
              error => this.errorMessage = error
            );
        },
        error => this.errorMessage = error
      );
  }

  saved(building : Building) {
    this.building = building;
    this.editing = false;
  }

}
