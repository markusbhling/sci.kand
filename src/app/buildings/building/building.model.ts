export class Building {

  public building_id? : number;
  public area_id? : number;
  public description_alias : string;
  public name_alias : string;
  public alias : string;
  public lat : number;
  public lng : number;
  public modtime? : string;
  public storage_dir? : string;

}

