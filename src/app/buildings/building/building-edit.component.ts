import {BuildingsService} from "../buildings.service";
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Building} from "./building.model";

@Component({
  selector: 'app-building-edit',
  templateUrl: './building-edit.component.html'
})
export class BuildingEditComponent implements OnInit {

  @Input() building: Building;
  @Output() cancelled = new EventEmitter();
  @Output() saved = new EventEmitter<Building>();

  private model : Building;
  errorMessage : string;

  constructor(private buildingsService : BuildingsService){}

  ngOnInit() {
    this.model = new Building();
    this.model.building_id = this.building.building_id;
    this.model.area_id = this.building.area_id;
    this.model.description_alias = this.building.description_alias;
    this.model.name_alias = this.building.name_alias;
    this.model.alias = this.building.alias;
    this.model.lat = this.building.lat;
    this.model.lng = this.building.lng;
    this.model.modtime = this.building.modtime;
    this.model.storage_dir = this.building.storage_dir;
  }

  submit() {
    this.buildingsService
      .saveBuilding(this.model)
      .subscribe(
        building => this.saved.emit(building),
        error => this.errorMessage = error
      );
  }

  cancel() {
    this.cancelled.emit();
  }

}
