import {BuildingsService} from "../buildings.service";
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Building} from "./building.model";
import {Router} from "@angular/router";

@Component({
  templateUrl: './building-new.component.html'
})
export class BuildingNewComponent implements OnInit {

  private model : Building;
  errorMessage : string;

  constructor(private buildingsService : BuildingsService, private router : Router){}

  ngOnInit() {
    this.model = new Building();
  }

  submit() {
    this.buildingsService
      .createBuilding(this.model)
      .subscribe(
        building => this.router.navigate(['/buildings', building.building_id]),
        error => this.errorMessage = error
      );
  }

}
