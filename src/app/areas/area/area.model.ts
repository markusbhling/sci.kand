export class Area {
  constructor(
    public area_id : number ,
    public parent_area_id : string ,
    public description_alias : string,
    public name_alias : string,
    public alias : string,
    public modtime : string,
    public storage_dir : string,
    public floor_order : number,
    public type : string,
    public center_x : string,
    public center_y : string,
    public bounds_x1 : string,
    public bounds_x2 : string,
    public bounds_y1 : string,
    public bounds_y2 : string
  ) {}
}

