import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AreasService} from "./areas.service";
import {AreasComponent} from "./areas.component";
import {HttpModule} from "@angular/http";

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [
    AreasComponent
  ],
  providers: [
    AreasService
  ]
})
export class AreasModule { }
