import {Injectable} from "@angular/core";
import {Http, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs";
import {Area} from "./area/area.model";


@Injectable()
export class AreasService {

  private areaUrl = "/api/areas";

  constructor(private http : Http) {}

  getAreaByParentId(parentAreaId : string) : Observable<Area[]> {
    let params = new URLSearchParams();
    params.set('parent_area_id', parentAreaId);
    return this.http.get(this.areaUrl, {search: params})
      .map(this.extractData);
  }


  private extractData(res: Response) {
    let body = res.json();
    return body || [];
  }
}
